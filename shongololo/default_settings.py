DATA_DIR: str = "./"
LOG_FILE: str = './shongololo_log.log'
DATA_FILE: str = 'data.csv'
PERIOD: float = 0.5
IMET_HEADINGS: str = ", IMET_ID, Pressure, Temperature, Humidity, Date, Time, Latitude x 1000000, Longitude x 1000000, Altitude x 1000, Sat Count"
K30_HEADINGS: str = ",K30_ID, CO2 ppm"
OUTPUT: str = "csv"
